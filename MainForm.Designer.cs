﻿/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2009-3-27
 * Time: 10:20
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace AutoWallpaper
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.label1 = new System.Windows.Forms.Label();
			this.btnSelect = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.btnPrevious = new System.Windows.Forms.Button();
			this.btnNext = new System.Windows.Forms.Button();
			this.btnPreview = new System.Windows.Forms.Button();
			this.btnApply = new System.Windows.Forms.Button();
			this.btnExit = new System.Windows.Forms.Button();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(75, 18);
			this.label1.TabIndex = 0;
			this.label1.Text = "图片路径：";
			// 
			// btnSelect
			// 
			this.btnSelect.Location = new System.Drawing.Point(155, 39);
			this.btnSelect.Name = "btnSelect";
			this.btnSelect.Size = new System.Drawing.Size(63, 23);
			this.btnSelect.TabIndex = 1;
			this.btnSelect.Text = "选择...";
			this.btnSelect.UseVisualStyleBackColor = true;
			this.btnSelect.Click += new System.EventHandler(this.BtnSelectClick);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(75, 12);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(222, 21);
			this.textBox1.TabIndex = 2;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.domainUpDown1);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Location = new System.Drawing.Point(12, 67);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(285, 173);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "设定";
			// 
			// domainUpDown1
			// 
			this.domainUpDown1.Items.Add("1");
			this.domainUpDown1.Items.Add("2");
			this.domainUpDown1.Items.Add("3");
			this.domainUpDown1.Items.Add("4");
			this.domainUpDown1.Items.Add("5");
			this.domainUpDown1.Items.Add("6");
			this.domainUpDown1.Items.Add("7");
			this.domainUpDown1.Items.Add("8");
			this.domainUpDown1.Items.Add("9");
			this.domainUpDown1.Items.Add("10");
			this.domainUpDown1.Items.Add("11");
			this.domainUpDown1.Items.Add("12");
			this.domainUpDown1.Items.Add("13");
			this.domainUpDown1.Items.Add("14");
			this.domainUpDown1.Items.Add("15");
			this.domainUpDown1.Items.Add("16");
			this.domainUpDown1.Items.Add("17");
			this.domainUpDown1.Items.Add("18");
			this.domainUpDown1.Items.Add("19");
			this.domainUpDown1.Items.Add("20");
			this.domainUpDown1.Location = new System.Drawing.Point(138, 28);
			this.domainUpDown1.Name = "domainUpDown1";
			this.domainUpDown1.Size = new System.Drawing.Size(42, 21);
			this.domainUpDown1.TabIndex = 0;
			this.domainUpDown1.Text = "10";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(63, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(157, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "循环时间：          分钟";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.pictureBox1);
			this.groupBox2.Location = new System.Drawing.Point(319, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(294, 231);
			this.groupBox2.TabIndex = 4;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "预览";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Location = new System.Drawing.Point(3, 17);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(288, 211);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// btnPrevious
			// 
			this.btnPrevious.BackColor = System.Drawing.SystemColors.Control;
			this.btnPrevious.Cursor = System.Windows.Forms.Cursors.PanWest;
			this.btnPrevious.Enabled = false;
			this.btnPrevious.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrevious.Location = new System.Drawing.Point(421, 249);
			this.btnPrevious.Name = "btnPrevious";
			this.btnPrevious.Size = new System.Drawing.Size(46, 18);
			this.btnPrevious.TabIndex = 5;
			this.btnPrevious.Text = "<<";
			this.btnPrevious.UseVisualStyleBackColor = false;
			this.btnPrevious.Click += new System.EventHandler(this.BtnPreviousClick);
			// 
			// btnNext
			// 
			this.btnNext.BackColor = System.Drawing.Color.Transparent;
			this.btnNext.Cursor = System.Windows.Forms.Cursors.PanEast;
			this.btnNext.Enabled = false;
			this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnNext.Location = new System.Drawing.Point(473, 249);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new System.Drawing.Size(45, 18);
			this.btnNext.TabIndex = 6;
			this.btnNext.Text = ">>";
			this.btnNext.UseVisualStyleBackColor = false;
			this.btnNext.Click += new System.EventHandler(this.BtnNextClick);
			// 
			// btnPreview
			// 
			this.btnPreview.Location = new System.Drawing.Point(234, 39);
			this.btnPreview.Name = "btnPreview";
			this.btnPreview.Size = new System.Drawing.Size(63, 23);
			this.btnPreview.TabIndex = 7;
			this.btnPreview.Text = "预览";
			this.btnPreview.UseVisualStyleBackColor = true;
			this.btnPreview.Click += new System.EventHandler(this.BtnPreviewClick);
			// 
			// btnApply
			// 
			this.btnApply.Location = new System.Drawing.Point(62, 249);
			this.btnApply.Name = "btnApply";
			this.btnApply.Size = new System.Drawing.Size(75, 23);
			this.btnApply.TabIndex = 8;
			this.btnApply.Text = "应用";
			this.btnApply.UseVisualStyleBackColor = true;
			this.btnApply.Click += new System.EventHandler(this.BtnApplyClick);
			// 
			// btnExit
			// 
			this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnExit.Location = new System.Drawing.Point(182, 249);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new System.Drawing.Size(75, 23);
			this.btnExit.TabIndex = 9;
			this.btnExit.Text = "退出";
			this.btnExit.UseVisualStyleBackColor = true;
			this.btnExit.Click += new System.EventHandler(this.BtnExitClick);
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "自动更换桌面壁纸";
			this.notifyIcon1.Click += new System.EventHandler(this.NotifyIcon1Click);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// MainForm
			// 
			this.AcceptButton = this.btnApply;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnExit;
			this.ClientSize = new System.Drawing.Size(309, 283);
			this.Controls.Add(this.btnExit);
			this.Controls.Add(this.btnApply);
			this.Controls.Add(this.btnPreview);
			this.Controls.Add(this.btnNext);
			this.Controls.Add(this.btnPrevious);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.btnSelect);
			this.Controls.Add(this.label1);
			this.Name = "MainForm";
			this.Text = "自动桌面壁纸";
			this.SizeChanged += new System.EventHandler(this.MainFormSizeChanged);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.DomainUpDown domainUpDown1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnExit;
		private System.Windows.Forms.Button btnApply;
		private System.Windows.Forms.Button btnPreview;
		private System.Windows.Forms.Button btnPrevious;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button btnSelect;
		private System.Windows.Forms.Label label1;
	}
}
