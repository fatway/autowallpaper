/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2009-3-27
 * Time: 17:20
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;

namespace AutoWallpaper
{
	/// <summary>
	/// Description of ConfigSetting.
	/// </summary>
	public class ConfigSetting
	{
		public ConfigSetting(int segtime)
		{
			this.segtime = segtime * 60 * 1000;
		}
		private int segtime = 0;
		public int SegTime
		{
			get{ return segtime; }
			set{}
		}
		private int totalnum = 0;
		public int TotalNum
		{
			get{ return totalnum; }
			set{ totalnum = value; }
		}
		private int rNum;
		public int RUnm
		{
			get
			{
				Random r = new Random();
				return r.Next(totalnum);
			}
		}
	}
}
