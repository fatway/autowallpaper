/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2009-3-27
 * Time: 10:42
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Collections;
using System.IO;

namespace AutoWallpaper
{
	/// <summary>
	/// Description of ImgCollection.
	/// </summary>
	public class ImgCollection
	{
		public ImgCollection()
		{
		}
		public ImgCollection(string filepath)
		{
			string[] filenames = Directory.GetFiles(filepath,"*.jpg");
			if(filenames.Length > 0)
			{
				this.imgcount = filenames.Length;
				foreach(string file in filenames)
				{
					this.img.Add(file);
				}
			}
		}
		private int imgcount = 0;
		public int ImgCount
		{
			get { return imgcount; }
			set {}
		}
		private ArrayList img = new ArrayList();
		public ArrayList Img
		{
			get { return img; }
		}
	}
}
