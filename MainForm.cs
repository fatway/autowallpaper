/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 2009-3-27
 * Time: 10:20
 *
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace AutoWallpaper
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class MainForm : Form
    {
        ImgCollection imgs = null;//parameter
        private static int currentNum = 0;
        ConfigSetting conf = null;
        
        [DllImport("user32.dll", EntryPoint = "SystemParametersInfoA")]
    	public static extern int SystemParametersInfo(int uAction ,int uParam ,string lpvParam ,int fuWinIni);
    
        public MainForm()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }

        void BtnPreviewClick(object sender, EventArgs e)//预览图像
        {
            if (this.btnPreview.Text == "预览")
            {
                if (textBox1.Text == string.Empty)
                    return;
                else
                {
                    this.Width = 633; //Set the formboder's width

                    this.imgs = new ImgCollection(textBox1.Text.ToString());

                    if (imgs.ImgCount == 0)
                        return;
                    if (imgs.ImgCount > 1)
                        this.btnNext.Enabled = true;
                    try
                    {
                        this.pictureBox1.Image = Image.FromFile(imgs.Img[0].ToString());
                    }
                    catch (Exception ee)
                    {
                        MessageBox.Show(ee.Message);
                    }
                    this.btnPreview.Text = "关闭";
                }
            }
            else
            {
                this.Width = 317;
                this.pictureBox1.Image = null;
//                this.imgs = null;
                this.btnNext.Enabled = false;
                this.btnPrevious.Enabled = false;
                currentNum = 0;
                this.btnPreview.Text = "预览";
            }
        }

        void BtnSelectClick(object sender, EventArgs e)//选择图像集合位置
        {
            FolderBrowserDialog imgCollection = new FolderBrowserDialog();
            imgCollection.Description = "选择照片存放路径,目前仅支持JPG图片";
            if (imgCollection.ShowDialog() == DialogResult.OK)
            {
                this.textBox1.Text = imgCollection.SelectedPath.ToString();
            }
        }

        void BtnNextClick(object sender, EventArgs e)
        {
            this.btnPrevious.Enabled = true;
            this.btnNext.Enabled = true;
            currentNum += 1;
            this.pictureBox1.Image = Image.FromFile(imgs.Img[currentNum].ToString());
            if (currentNum == imgs.ImgCount - 1)
            {
                btnNext.Enabled = false;
                return;
            }
        }

        void BtnPreviousClick(object sender, EventArgs e)
        {
            this.btnPrevious.Enabled = true;
            this.btnNext.Enabled = true;
            currentNum -= 1;
            this.pictureBox1.Image = Image.FromFile(imgs.Img[currentNum].ToString());
            if (currentNum == 0)
            {
                btnPrevious.Enabled = false;
                return;
            }
        }
		
		void BtnExitClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void Timer1Tick(object sender, EventArgs e)
		{
//			string file = imgs.Img[conf.RUnm].ToString();
//			string filepath = file;
//			string[] chars = file.Split('.');
//			Bitmap bm = new Bitmap(file);
//			if(chars[1].ToString().ToLower() != "bmp")
//			{
//				filepath = chars[0] + ".bmp";
//				bm.Save(filepath);
//			}
			string file = imgs.Img[conf.RUnm].ToString();
			string filepath = file;
			int nResult ;
       		if (File.Exists(filepath))
       		{
        		nResult = SystemParametersInfo(20, 1, filepath, 1);
        		if(nResult==0)
         			MessageBox.Show("没有更新成功!");
       		}
       		else
        		MessageBox.Show("文件不存在!");
		}
		
		void 退出ToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void 主程序ToolStripMenuItemClick(object sender, EventArgs e)
		{
			
		}
		
		void NotifyIcon1Click(object sender, EventArgs e)
		{
			if(this.WindowState == FormWindowState.Minimized)
			{
				this.Visible = true;
				this.WindowState = FormWindowState.Normal;
				this.notifyIcon1.Visible = false;
			}
		}
		
		void MainFormSizeChanged(object sender, EventArgs e)
		{
			if(this.WindowState == FormWindowState.Minimized)
			{
				this.Hide();
				this.notifyIcon1.Visible = true;
			}
		}
		
		void BtnApplyClick(object sender, EventArgs e)
		{
			if(this.imgs == null)
			{
				MessageBox.Show("开玩笑的吧大哥，目标集合中并无图像","7thSpace", MessageBoxButtons.OK,MessageBoxIcon.Error);
				return;
			}
			conf = new ConfigSetting(int.Parse(domainUpDown1.Text.ToString()));
			this.timer1.Interval = conf.SegTime;
			conf.TotalNum = imgs.ImgCount;
			timer1.Enabled = true;
			this.WindowState = FormWindowState.Minimized;
		}
    }
}

